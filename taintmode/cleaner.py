import re

def c_user(s):
    if re.match('[a-z]+\@domain.se$',s): return s
    print('Sanatization for username failed!')
    quit()

def c_file(s):
    if re.search('\.\.',s) or re.search('^/',s) or re.search(';',s):
       print('Sanatization for the requested file failed!')
       quit()
    return s
