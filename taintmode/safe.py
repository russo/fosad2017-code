# This is how unsafe.py needs to be adapted to run taint analysis
#
# Now, try to run
#   python safe.py ale@domain.se 'file.txt ; echo attack'
#
# You should get an error of sanitization

import sys
import os
# Importing the taint analysis
from taintmode import *
# Importing the sanitization functions
from cleaner import *

# Indicating the sensitive sink
os.system = ssink(OSI)(os.system)

# Indicating the sanitization functions
s_usermail = cleaner(OSI)(c_user)
s_file = cleaner(OSI)(c_file)

# Indicating the contaminated input
usermail = taint(sys.argv[1],OSI)
file = taint(sys.argv[2],OSI)

#
#
#
# Imagine that here is plenty of code
#
#
#

# Doing the sanitization
usermail = s_usermail(usermail)
file = s_file(file)
# If you comment any of the two lines above, you should
# get a complain from the analysis. Try it out!


cmd = './mail -s "Re: your file" ' + usermail + ' < ' + file
print 'Running ----> ' + cmd
os.system(cmd)
