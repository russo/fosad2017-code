interface Label : 
	Label Label(String)
	Label and(String or Label)
	Label or(String or Label)
	bool subsumes (Label, [Privilege])
	